package com.tw.migrations_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MigrationsDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MigrationsDemoApplication.class, args);
    }

}
